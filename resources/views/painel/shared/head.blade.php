
<link rel="stylesheet" href="{{asset('assets/jquery-ui-1.12.1.custom/jquery-ui.css') }}">
<script src="{{ asset('assets/jquery-ui-1.12.1.custom/external/jquery/jquery.js') }}"></script>
<script src="{{ asset('assets/jquery-ui-1.12.1.custom/jquery-ui.js') }}"></script>
<link rel="stylesheet" href="{{asset('assets/jquery-ui-1.12.1.custom/jquery-ui.theme.css') }}">

<link rel="stylesheet" href="{{asset('assets/bootstrap-4.5/css/bootstrap.css') }}">
<link rel="stylesheet" href="{{asset('assets/chktecnologia/css/chktecnologia.css') }}">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw==" crossorigin="anonymous" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ==" crossorigin="anonymous"></script>

{{-- <script type="text/javascript" src="{{ asset('assets/crypto-js/crypto-js.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/bcrypt/bcrypt/impl.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/bcrypt/bcrypt/util/utf8.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/bcrypt/bcrypt/util/base64.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/bcrypt/bcrypt/util.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/bcrypt/bcrypt.js') }}"></script> --}}
<script type="text/javascript" src="{{ asset('assets/bcrypt/sha1.js') }}"></script>
{{-- <script type="text/javascript" src="/js/app.js"></script> --}}

<link rel="stylesheet" href="{{asset('assets/Alertjs/css/Alert.css') }}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css"
integrity="sha256-h20CPZ0QyXlBuAw7A+KluUYx/3pK+c7lYEpqLTlxjYQ=" crossorigin="anonymous" />

<script src="{{ asset('assets/moment/js/moment.js') }}"></script>
<script src="{{ asset('assets/maskedinput/maskedinput.js') }}"></script>
<script src="{{asset('assets/chktecnologia/js/validadorCNPJ.js') }}"></script>
<script src="{{asset('assets/chktecnologia/js/validadorCPF.js') }}"></script>
<script src="{{ asset('assets/knockoutjs-3.5.0/knockout-3.5.0.js') }}"></script>
<script src="{{ asset('assets/knockoutjs-3.5.0/knockout.validation.js') }}"></script>
<script src="{{ asset('assets/knockoutjs-3.5.0/knockout-custon-bindings.js') }}"></script>
<script src="{{ asset('assets/knockstrap/knockstrap.js') }}"></script>
<script src="{{ Route('dynamicjs.lang.js') }}"></script>
<script src="{{ Route('dynamicjs.base.js') }}"></script>
<script src="{{asset('assets/Alertjs/js/Alert.js')}}"></script>

<link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

