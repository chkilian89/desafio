<?php

namespace App\Services;

use App\Exceptions\NegocioException;
use App\Traits\HelperTrait;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class BaseService
{
    use HelperTrait;
}
