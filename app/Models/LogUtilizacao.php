<?php

namespace App\Models;

use App\Models\BaseModel;

class LogUtilizacao extends BaseModel
{
    protected $table = "log_utilizacao";
    protected $primaryKey = "lga_id";
}
